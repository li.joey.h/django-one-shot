from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists": todolists
    }

    return render(request, "todos/todoslist.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todos_object": todos,
    }

    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.iz_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_todo.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=todo)

    context = {
        "todo": todo,
        "form": form

    }

    return render(request, "todos/edit_todo.html", context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    else:
        form = TodoForm()

    context = {
        "form": form
    }

    return render(request, 'todos/delete_todo.html', context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save
            return redirect("todo_list_detail", id=new_item().list.id)

    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/todo_items_create.html", context)
